package exservlet;
import exservlet.model.Post;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/hello")
public class HelloServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.getWriter().print("Hello!");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String json = Util.readStream(request.getInputStream());
            Post post = JsonConverter.jsonToPost(json);
            post.setTitle("Success!!");
            String changed = JsonConverter.postToJson(post);
            response.setContentType("json");
            response.getWriter().append(changed);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
