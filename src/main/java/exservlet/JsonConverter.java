package exservlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import exservlet.model.Post;

import java.io.StringWriter;
import java.util.Arrays;

public class JsonConverter {

    public static void main(String[] args) throws Exception {

        Post post = new Post();
        post.setId(1L);
        post.setTitle("Covert Json");
        post.setHidden(false);
        post.setTags(Arrays.asList("java", "json"));

    }

    public static String postToJson(Post post) {
        try {
            ObjectMapper objmap = new ObjectMapper();
            objmap.configure(SerializationFeature.INDENT_OUTPUT, true);

            //writing to console, can write to any output stream such as file
            StringWriter stringPost = new StringWriter();
            objmap.writeValue(stringPost, post);

            return stringPost.toString();
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Post jsonToPost(String json) {
        try {
            ObjectMapper objmap = new ObjectMapper();
            return objmap.readValue(json, Post.class);
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }


}
